- [Introduction](#introduction)
- [Before we begin](#before-we-begin)
- [Assumption](#assumption)
- [Name](#name)
- [Parameters](#parameters)
- [Drawback/Limitation](#drawbacklimitation)
- [Licence](#licence)
- [References](#references)
- [Change log](#change-log)

# Introduction
In this module we developed a new powershell cmdlet to validate a Json file with Json schema.
We have used Newtonsoft Json Schema Nuget package version 3.0.14.
The code is written for target framework .Net 4.8.

# Before we begin
We have used the following resources to develop this module.
- Microsoft Visual Studio 2022 Community edition.
- Import the followiwng Nuget packages - 
  - Microsoft.Powershell.5.1.ReferenceAssemblies.
  - NewtonsSoft.Json.Schema version 3.0.14
- Set the target framework to Microsoft .Net Framework 4.8
- 

# Assumption
None

# Name
Test-JsonSchema

# Parameters

Variable|IsRequired|DataType|Description
--------|----------|--------|------------
Json|Required|String|Specifies the Json data.
Schema|Required|String|Specifies the schema. 

# Drawback/Limitation
- The cmdlet accepts Json data and schema data only. It does  not read json or schema files.

# Licence
The source code is licenced under GNU GPL v3.0.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Please report any issues at [here](mailto:incoming+mcquickstart-azure-quickstart-35530128-eyfyukiyfr58mp236sye2gxkf-issue@incoming.gitlab.com).
Alternatively, you can send me an email [here](mcquickstart@outlook.com).

# References
1. None
# Change log
Version|Date|Description
-------|----|-----------|
1.0.0.0|2022-06-12| Initial version.
1.0.0.1|2022-06-12| Convert the class to Powershell 5.1 compatible cmdlet.
1.0.0.2|2022-06-17| Add functionality to honor common parameters.

