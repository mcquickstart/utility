﻿// ---------------------License Information----------------------------------
// MIT License
// Copyright (c) 2022 Multicloud Quickstart
//
// Please refer to the ReadMe.md file for contact details.
// ---------------------------Change Log----------------------------------------
// 2022-06-12: Version 1.0.0.0: Initial version.
// 2022-06-12: Version 1.0.0.1: Convert the class to Powershell 5.1 compatible cmdlet.
// 2022-06-17: Version 1.0.0.2: Add functionality to honor common parameters.


using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.Collections.Generic;
using System.Text;
using System.Management.Automation;


namespace MCQ.Utility.Json
{
    /// <summary>
    /// Validate the JSON.
    /// </summary>
    [Cmdlet(VerbsDiagnostic.Test, "JsonSchema")]
    [OutputType(typeof(ValidateResponse))]
    public class TestJsonSchemaCmdlet : Cmdlet
    {
        /// <summary>
        /// Input JSON file
        /// </summary>
        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the Json data.",
            ValueFromPipeline = true,
            ValueFromPipelineByPropertyName = true)]
        [Alias("J")]
        public string Json { get; set; }
        /// <summary>
        /// Input Schema file.
        /// </summary>
        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the schema.",
            ValueFromPipeline = true,
            ValueFromPipelineByPropertyName = true)]
        [Alias("S")]
        public string Schema { get; set; }

        private ValidateResponse validateResponse;

        protected override void BeginProcessing()
        {
            base.BeginProcessing();
        }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            validateResponse = Validate(Json, Schema);

            WriteObject(validateResponse);
        }

        protected override void EndProcessing()
        {
            base.EndProcessing();
            Json = null;
            Schema = null;
        }

        /// <summary>
        /// This function will validate a JSON  file against a schema.
        /// </summary>
        /// <param name="jsonText">json data to validate</param>
        /// <param name="schemaText">json  schema to validate against</param>
        /// <returns>Response message</returns>
        public static ValidateResponse Validate(string jsonText, string schemaText)
        {
            //Load the schema
            JSchema schema = JSchema.Parse(schemaText);
            //Load the JSON file
            JToken token = JToken.Parse(jsonText);

            // validate json
            IList<ValidationError> errors;
            bool valid = token.IsValid(schema, out errors);

            // return error messages and line info to the browser
            return new ValidateResponse
            {
                IsValid = valid,
                Errors = errors
            };
        }
    }

    //Response Parameters
    public class ValidateResponse
    {
        /// <summary>
        /// Denotes whether the json file is valid.
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// List of errors.
        /// </summary>
        public IList<ValidationError> Errors { get; set; }

        /// <summary>
        /// Display the error messages.
        /// </summary>
        /// <returns>Display the error messages</returns>
        public override string ToString()
        {
            StringBuilder sBuider = new StringBuilder();
            foreach (var item in Errors)
            {
                sBuider.AppendLine(
                        string.Format("Line {0}: Position: {1}: Value: {2}: {3}",
                                item.LineNumber.ToString(),
                                item.LinePosition.ToString(),
                                item.Value.ToString(),
                                item.Message));
            }

            return sBuider.ToString();
        }
    }
}
